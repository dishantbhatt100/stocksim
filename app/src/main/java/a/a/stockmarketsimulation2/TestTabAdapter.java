package a.a.stockmarketsimulation2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class TestTabAdapter extends FragmentStatePagerAdapter {
    int no_of_tabs;

    public TestTabAdapter(FragmentManager fm, int no_of_tabs) {
        super(fm);
        this.no_of_tabs = no_of_tabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0 :
                return new SimulationTabFragment();

            case 1 :
                return new OrderHistoryTabFragment();

            case 2 :
                return new OrderHistoryTabFragment();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return no_of_tabs;
    }
}
