package a.a.stockmarketsimulation2;

import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TestTabActivity extends AppCompatActivity {

    ViewPager simpleViewPager;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_tab);
        init();
        setListener();
    }

    private void init()
    {
        simpleViewPager = (ViewPager) findViewById(R.id.viewpager_testtab);
        tabLayout = (TabLayout) findViewById(R.id.tablayout_testtab);

        TabLayout.Tab t1 = tabLayout.newTab();
        t1.setText("Simulation");
        TabLayout.Tab t2 = tabLayout.newTab();
        t2.setText("Order");
        TabLayout.Tab t3  = tabLayout.newTab();
        t3.setText("History");
        tabLayout.addTab(t1);
        tabLayout.addTab(t2);
        tabLayout.addTab(t3);
        tabLayout.setTabTextColors(Color.parseColor("#FF2D80BA"),Color.BLACK);
        tabLayout.setSelectedTabIndicatorColor(Color.BLACK);
        tabLayout.setBackgroundColor(Color.parseColor("#FFDDE9FC"));
        TestTabAdapter testTabAdapter = new TestTabAdapter(getSupportFragmentManager(),tabLayout.getTabCount());
        simpleViewPager.setAdapter(testTabAdapter);
        simpleViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }


    private void setListener() {
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(simpleViewPager));

    }
}
